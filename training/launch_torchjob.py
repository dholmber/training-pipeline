'''
1. Launch PyTorchJob
2. Wait for completion
3. Delete TFJob
'''

import argparse
import kubernetes
from kubernetes import client
import kfp
import time
import uuid
import os

print('Launching PyTorchJob')

print('Parsing args')

parser = argparse.ArgumentParser(description='ML Trainer')
parser.add_argument('--num_epochs')
parser.add_argument('--batch_size')
parser.add_argument('--start_lr')
parser.add_argument('--optimizer')
parser.add_argument('--num_workers')
parser.add_argument('--fetch_step')
parser.add_argument('--data_fraction')
parser.add_argument('--data_train')
parser.add_argument('--data_test')
parser.add_argument('--data_config')
parser.add_argument('--network_config')
parser.add_argument('--model_prefix')
parser.add_argument('--log')
parser.add_argument('--predict_output')
parser.add_argument('--torchjob_image')
args = parser.parse_args()

torchjob_name = 'torch-test'
config_map = f'torchjob-config-map-{uuid.uuid4()}'
namespace = kfp.Client().get_user_namespace()
network_name = os.path.basename(args.network_config)

torchjob = {
    'apiVersion': 'kubeflow.org/v1',
    'kind': 'PyTorchJob',
    'metadata': {
        'name': torchjob_name,
        'namespace': namespace
    },
    'spec': {
        'pytorchReplicaSpecs': {
            'Master': {
                'replicas': 1,
                'template': {
                    'spec': {
                        'volumes': [
                            {
                                'name': 'eos',
                                'hostPath': {
                                   'path': '/var/eos'
                                }
                            },
                            {
                                'name': 'krb-secret-vol',
                                'secret': {
                                    'secretName': 'krb-secret'
                                }
                            }
                        ],
                        'containers': [
                            {
                                'resources': {
                                    'limits': {
                                        'nvidia.com/gpu': 1
                                    }
                                },
                                'volumeMounts': [
                                    {
                                       'name': 'eos',
                                       'mountPath': '/eos'
                                    },
                                    {
                                       'name': 'krb-secret-vol',
                                       'mountPath': '/secret/krb-secret-vol'
                                    }
                                ],
                                'image': args.torchjob_image,
                                'name': 'pytorch',
                                'command': ['sh', '-c'],
                                'args': [
                                    ' '.join([
                                        'cp /secret/krb-secret-vol/krb5cc_1000 /tmp/krb5cc_1000;',
                                        'chmod 600 /tmp/krb5cc_1000;',
                                        f'cp {args.network_config} .;',
                                        'python train.py',
                                        '--network-config',
                                        network_name,
                                        '--data-train',
                                        args.data_train,
                                        '--data-test',
                                        args.data_test,
                                        '--data-config',
                                        args.data_config,
                                        '--model-prefix',
                                        args.model_prefix,
                                        '--predict-output',
                                        args.predict_output,
                                        '--log',
                                        args.log,
                                        '--num-workers',
                                        args.num_workers,
                                        '--fetch-step',
                                        args.fetch_step,
                                        '--data-fraction',
                                        args.data_fraction,
                                        '--batch-size',
                                        args.batch_size,
                                        '--start-lr',
                                        args.start_lr,
                                        '--optimizer',
                                        args.optimizer,
                                        '--num-epochs',
                                        args.num_epochs,
                                        '--gpus',
                                        '0',
                                    ])
                                ]
                            }
                        ],
                        'restartPolicy': 'Never'
                    },
                    'metadata': {
                        'annotations': {
                            'sidecar.istio.io/inject': 'false'
                        }
                    }
                }
            }
        }
    }
}

kubernetes.config.load_incluster_config()
print('Incluster config loaded')

k8s_co_client = kubernetes.client.CustomObjectsApi()
print('CO client obtained')

k8s_co_client.create_namespaced_custom_object(
    group='kubeflow.org',
    version='v1',
    namespace=namespace,
    plural='pytorchjobs',
    body=torchjob
)

while True:
    time.sleep(5)
    
    resource = k8s_co_client.get_namespaced_custom_object(
        group='kubeflow.org',
        version='v1',
        namespace=namespace,
        plural='pytorchjobs',
        name=torchjob_name
    )
    
    master = resource['status']['replicaStatuses']['Master']
    print(master)
    
    if 'failed' in master:
        break
        sys.exit(1)
        
    if 'succeeded' in master:
        print('Great Success')
        break

print('Results written')

k8s_co_client.delete_namespaced_custom_object(
    group='kubeflow.org',
    version='v1',
    namespace=namespace,
    plural='pytorchjobs',
    name=torchjob_name,
    body=client.V1DeleteOptions()
)

print(f'PyTorchJob {torchjob_name} deleted')
